package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.assertTrue;
import com.agiletestingalliance.Usefulness;

public class UsefulnessTest {
    @Test
    public void testDescReturnType() {
        final Usefulness usefulness = new Usefulness();
        assertTrue(usefulness.desc() instanceof String);
    }

    @Test
    public void testFunctionWF() {
        final Usefulness usefulness = new Usefulness();
        usefulness.functionWF();
    }
}
