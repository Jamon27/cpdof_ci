package com.agiletestingalliance;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

import com.agiletestingalliance.MinMax;

public class MinMaxTest {
    @Test
    public void testCompareValues() {
        final MinMax minMax = new MinMax();
        final int first = 5;
        final int second = 10;
        final int expected = 10;

        assertEquals(expected, minMax.compareValues(first, second));
    }

    @Test
    public void testBar() {
        final MinMax minMax = new MinMax();
        final String str = "Hello, World!";
        final String expected = "Hello, World!";

        assertEquals(expected, minMax.bar(str));
    }

    @Test
    public void testBarWithEmptyString() {
        final MinMax minMax = new MinMax();
        final String str = "";
        final String expected = "";

        assertEquals(expected, minMax.bar(str));
    }

    @Test
    public void testBarWithNullString() {
        final MinMax minMax = new MinMax();
        final String str = null;
        final String expected = null;

        assertEquals(expected, minMax.bar(str));
    }
}
