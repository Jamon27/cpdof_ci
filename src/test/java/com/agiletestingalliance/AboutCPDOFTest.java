package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.assertTrue;
import com.agiletestingalliance.AboutCPDOF;

public class AboutCPDOFTest {
    @Test
    public void testDescReturnType() {
        final AboutCPDOF aboutCPDOF = new AboutCPDOF();
        assertTrue(aboutCPDOF.desc() instanceof String);
    }
}
