package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.assertTrue;
import com.agiletestingalliance.Duration;

public class DurationTest {
    @Test
    public void testDescReturnType() {
        final Duration duration = new Duration();
        assertTrue(duration.dur() instanceof String);
    }
}
